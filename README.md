# Automated Terminate Older Aws Instance
Automated terminate older aws instance to make the platform team’s life easier.

## Program Workflow
1. Check the development servers running on the AWS.
2. Each server has its specific code-base, corresponding to a feature branch in Github.
3. Check the last commit on that Github branch, and if it’s older than three days, terminate the server.

## Preparation & Assumptions
Need to add tags when launching an instance.

|Key|Value|
|---|---
|gitHubUser|GitHub Username (ex: AlstonWu)
|gitHubRepo|GitHub Repository (ex: awstest)
|gitHubBranch|GitHub Branch (ex: test-v1)
|olderThanDays|Older Than n Days (ex: 3)

### Example:
* GitHub Repository: https://github.com/AlstonWu/awstest/tree/test-v1
* Add tags when launching an instance

![example](https://docs.google.com/uc?id=1S9DhMICZS0yYxHSkTsOSw5YmR2fH1YWv "Add Tags")

## Running cron jobs on AWS Lambda
### Step 1. Sign in AWS Lambda
https://console.aws.amazon.com/lambda

### Step 2. Create function
* Choose Runtime "Python 3.6"
* Choose Role "Create a new role from one or more templates."
    * This new role will be scoped to the current function. To use it with other functions, you can modify it in the IAM console.

![Author from scratch](https://docs.google.com/uc?id=1M5sGQfuBDyLg1ANN1Im-5ZnnMmndhFpk "Author from scratch")

### Step 3. Add triggers
* Choose "CloudWatch Events"

![CloudWatch Events](https://docs.google.com/uc?id=1E4p48TZClsGIc3dIqnW5S90gp74K0gjL "CloudWatch Events")

### Step 4. Configure triggers
* Invoke a Lambda function at 00:00am (UTC) everyday
    * Schedule expression: cron(0 0 * * ? *)

![Configure triggers](https://docs.google.com/uc?id=15lHUO2CPyPDTaot3dFvq3WRUiwfXDWoW "Configure triggers")

### Step 5. Function code
* Copy paste from [lambda_function.py](/lambda_function.py)

![Function code](https://docs.google.com/uc?id=1KruQ3PRjL20RB7M6rNVtwgXgJQGumbzo "Function code")

### Step 6. Modify it in the IAM console
https://console.aws.amazon.com/iam/home#/roles
* Choose your role (ex: lambdaForLifeEasier)
* Attach policies
    * AmazonEC2SpotFleetTaggingRole
    * AmazonEC2ContainerServiceFullAccess

![Attach policies](https://docs.google.com/uc?id=15Txy-6-pRxF2A-Kfpr68spLWKKcB0wGd "Attach policies")

### Step 7. Save and Test
![Execution result: succeeded](https://docs.google.com/uc?id=1e7UINYhOWwD2sWsjQIyyZXK_qs4ry53v "Execution result: succeeded")

## License
This project is licensed under the MIT License.
