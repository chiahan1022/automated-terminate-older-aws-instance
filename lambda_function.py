import boto3
from botocore.vendored import requests
import datetime

def terminateOlderAwsInstance():
    client = boto3.client('ec2')
    response = client.describe_instances()
    instanceInfoList = []
    tmp = []
    for i in range(len(response['Reservations'])):
        data = response['Reservations'][i]['Instances'][0]
        Tags = data.get('Tags')
        status = data.get('State').get('Name')
        if 'running' == status :
            instanceInfo = {}
            instanceId = data.get('InstanceId')
            instanceInfo['InstanceId'] = instanceId
            if Tags:
                tag_values = ['gitHubUser','gitHubRepo','gitHubBranch','olderThanDays']
                tag_keys = list(Tags[0].keys())
                tmp = [i for i in Tags if i['Key'] in tag_values]
            for i in tmp:
                instanceInfo[i['Key']] = i['Value']
            instanceInfoList.append(instanceInfo)
    instanceIds = checkAwsInstanceOlderThanDay(instanceInfoList)
    if instanceIds:
        terminateAwsInstance(client,instanceIds)

def queryGitRepositoryLastCommitDate(user,repo,branch): 
    request = requests.get('https://api.github.com/repos/'+user+'/'+repo+'/commits/'+branch)
    if request.status_code == 200:
        lastCommitDate = request.json()['commit']['committer']['date']
        return lastCommitDate
    else:
        raise Exception("Query failed to run by returning code of {}. {}".format(request.status_code, query))
                
def checkAwsInstanceOlderThanDay(instanceInfoList):
    eDate = datetime.datetime.utcnow()
    instanceIds = []
    for i in instanceInfoList :
        day = int(i['olderThanDays'])
        instanceId = i['InstanceId']
        startDate = queryGitRepositoryLastCommitDate(i['gitHubUser'],i['gitHubRepo'],i['gitHubBranch'])
        sDate = datetime.datetime.strptime(startDate, '%Y-%m-%dT%H:%M:%SZ')
        delta = eDate - sDate
        if delta.days > day :
            instanceIds.append(instanceId)
            print('Last commit date: '+str(sDate)+'('+str(delta.days)+' day ago). '\
            'It is older than '+str(day)+' days, so the instance('+instanceId+') is going to be terminated.')
        else:
            print('Last commit date: '+str(sDate)+'('+str(delta.days)+' day ago). '\
            'It is not older than '+str(day)+' days, so the instance('+instanceId+') was not terminated.')
    return instanceIds

def terminateAwsInstance(client,instanceIds):
    response = client.terminate_instances(InstanceIds=instanceIds)
        
def lambda_handler(event, context):
    terminateOlderAwsInstance()
